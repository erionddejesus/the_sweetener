package com.example.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.myapplication.ui.main.SectionsPagerAdapter;

public class TheSweetener extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_sweetener);


        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Place your order now!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.fab);
                myFab.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        setContentView(R.layout.order_layout);;
                    }
                });
            }
        });
    }

    private int quantity = 1;
    private int price = 180;

    public void displayQuantity (int n)
    {
        TextView text = (TextView)  findViewById(R.id.quantity_value);
        text.setText("" + n);
    }

    public void displayPrice (int n)
    {
        TextView text = (TextView)  findViewById(R.id.summary_value);
        text.setText("" + n);
    }


    public void increment(View view){
        quantity++;
        displayQuantity(quantity);
    }

    public void decrement(View view){
        quantity--;
        displayQuantity(quantity);
    }

    public boolean isWhippedCreamChecked ()
    {
        CheckBox check = (CheckBox) findViewById(R.id.cream_box);

        if (check.isChecked())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isChocolateChecked ()
    {
        CheckBox check = (CheckBox) findViewById(R.id.chocolate_box);

        if (check.isChecked())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int calculatePrice (boolean checkCream, boolean checkChocolate){

        int price2 = 180;

        if (!checkCream && !checkChocolate)
        {
            return price = price2 * quantity;
        }

        if (checkCream && !checkChocolate)
        {
            return price = 40 + (quantity * price2);
        }

        if (!checkCream && checkChocolate)
        {
            return price = 50 + (quantity * price2);
        }
        if (checkCream && checkChocolate)
        {
            return price = 90 + (quantity * price2);
        }
        else
        {
            return price2;
        }

    }

    public String CreateOrderSummary (int price, boolean addWhippedCream, boolean addChocolate)
    {
        return ("Name: Eriond De Jesus " + "\n Add whipped cream? " + addWhippedCream + "\n Add chocolate? " + addChocolate + "\n Quantity: " + quantity + " \n Total: " + price + "\n Thank you!");
    }

    public void displayMessage (String message)
    {
        TextView text = (TextView)findViewById(R.id.summary_value);
        text.setText("" + message);
    }

    public void submitOrder (View v)
    {
        calculatePrice(isWhippedCreamChecked(), isChocolateChecked());
        CreateOrderSummary(price, isWhippedCreamChecked(), isChocolateChecked());
        displayMessage(CreateOrderSummary(price, isWhippedCreamChecked(), isChocolateChecked()));

    }

    public void reset2 (View v) {
        quantity = 1;
        price = 180;

        displayQuantity(quantity);
        displayPrice(price);

    }

    public void onClick(View view) {
        finish();
    }
}